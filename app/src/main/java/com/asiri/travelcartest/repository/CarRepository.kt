package com.asiri.travelcartest.repository

import androidx.lifecycle.LiveData
import com.asiri.travelcartest.App
import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.model.CarFromServer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.Executors

class CarRepository {

    private val carUrl =
        "https://gist.githubusercontent.com/ncltg/6a74a0143a8202a5597ef3451bde0d5a/raw/8fa93591ad4c3415c9e666f888e549fb8f945eb7/"
    private val retrofit = Retrofit.Builder()
        .baseUrl(carUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    //Function for car http call
    fun syncCarNow() {

        //Prepare and execute the service
        val service = retrofit.create(CarApi::class.java)
        service.getCars().enqueue(object : Callback<List<CarFromServer>> {
            override fun onResponse(
                call: Call<List<CarFromServer>>,
                response: Response<List<CarFromServer>>
            ) {
                Timber.d("We got a response from the car api")
                val carsFromServer = response.body()

                //Inserting all cars
                Executors.newSingleThreadExecutor().execute {
                    if (carsFromServer != null) {
                        //We have to delete all the old cars because the api doesn't give an unique id to be able to update the internal data
                        App.db.carDao().deleteAllCars()

                        for (carFromServer in carsFromServer) {
                            App.db.carDao().insertCar(carFromServer.toCar())
                        }
                        Timber.d("%d cars were found", carsFromServer.size)
                    }
                }
            }

            override fun onFailure(call: Call<List<CarFromServer>>, t: Throwable) {
                Timber.e("HTTP GET fail")
                Timber.e(t)
            }
        })
        Timber.d("Executing an car HTTP GET")
    }

    fun getAllCars(): LiveData<List<Car>> {
        return App.db.carDao().getAllCars()
    }

    fun getCarsFromName(name: String): LiveData<List<Car>> {
        return App.db.carDao().getCarsWhereNameContains("%$name%")
    }

}



private fun CarFromServer.toCar() = Car(carId, make, model, year, picture, flattenEquipments())

private fun CarFromServer.flattenEquipments(): String {
    var equipmentsString = ""
    if (equipments != null)
        for (equipment in equipments) {
            equipmentsString = equipmentsString.plus("$equipment,")
        }
    return equipmentsString
}


