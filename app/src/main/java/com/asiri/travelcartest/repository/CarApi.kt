package com.asiri.travelcartest.repository

import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.model.CarFromServer
import retrofit2.Call
import retrofit2.http.GET


interface CarApi {

    @GET("tc-test-ios.json")
    fun getCars(): Call<List<CarFromServer>>
}