package com.asiri.travelcartest.ui.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import com.asiri.travelcartest.R
import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.ui.search.SearchFragment.Companion.CAR_CONTEXT_KEY
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_car_details.*

class CarDetailsActivty : AppCompatActivity() {

    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_details)
        supportActionBar?.hide()

        car = intent?.getSerializableExtra(CAR_CONTEXT_KEY) as Car

        //Image loading via Picasso
        Picasso.get()
            .load(car.picture)
            .placeholder(R.drawable.ic_default_car)
            .into(car_picture)

        car_title.text = "${car.make} ${car.model}"
        car_year.text = "${car.year}"
        val equipmentsAdapter = ArrayAdapter<String>(this,
            R.layout.item_equipment, car.equipmentsString.split(","))
        car_equipments.adapter = equipmentsAdapter
    }


}
