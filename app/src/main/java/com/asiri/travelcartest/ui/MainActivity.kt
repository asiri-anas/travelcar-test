package com.asiri.travelcartest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.asiri.travelcartest.R
import com.asiri.travelcartest.ui.account.AccountFragment
import com.asiri.travelcartest.ui.helper.ViewPagerAdapter
import com.asiri.travelcartest.ui.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewPager()
    }

    /**
     * Setup the view pager with the fragments and the title.
     */
    private fun setupViewPager() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

        viewPagerAdapter.addFragment(SearchFragment(), resources.getString(R.string.search_title))
        viewPagerAdapter.addFragment(AccountFragment(), resources.getString(R.string.account_title))

        viewPager.adapter = viewPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

}
