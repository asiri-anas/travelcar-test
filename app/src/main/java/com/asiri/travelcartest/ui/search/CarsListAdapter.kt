package com.asiri.travelcartest.ui.search

import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.asiri.travelcartest.R
import com.asiri.travelcartest.model.Car
import com.squareup.picasso.Picasso
import android.text.Html
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import java.util.*


class CarsListAdapter(
    private val cars: List<Car>,
    private val listener: CarsListAdapterListener?
) : RecyclerView.Adapter<CarsListAdapter.ViewHolder>(), View.OnClickListener {

    var query = ""

    interface CarsListAdapterListener {
        fun onCarSelected(car: Car)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val carView = itemView.findViewById<ConstraintLayout>(R.id.car_view)!!
        val carImage = itemView.findViewById<ImageView>(R.id.car_picture)!!
        val carTitle = itemView.findViewById<TextView>(R.id.car_title)!!
        val carYear = itemView.findViewById<TextView>(R.id.car_year)!!
        val carEquipments = itemView.findViewById<ListView>(R.id.car_equipments)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_car, parent, false)
        return ViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val car = cars[position]
        with(holder) {

            //Update the car elements and add click listener
            carView.setOnClickListener(this@CarsListAdapter)
            carView.tag = car
            carTitle.text = applyHighlight(car, query)

            carYear.text = "${car.year}"
            val equipmentsAdapter = ArrayAdapter<String>(itemView.context,
                R.layout.item_equipment_end, car.equipmentsString.split(","))
            carEquipments.adapter = equipmentsAdapter

            //Image loading via Picasso
            Picasso.get()
                .load(car.picture)
                .placeholder(R.drawable.ic_default_car)
                .into(carImage)
        }

    }

    private fun applyHighlight(car: Car, selection: String) : SpannableString {
        val text = "${car.make.toLowerCase(Locale.ROOT)} ${car.model.toLowerCase(Locale.ROOT)}"
        val index = text.indexOf(selection)
        if (index != -1) {
            val result = SpannableString(text)
            result.setSpan( BackgroundColorSpan(Color.YELLOW), index, index + selection.length, 0)
            return result
        } else {
            return SpannableString(text)
        }
    }

    override fun getItemCount() = cars.size

    override fun onClick(view: View) {
        when (view.id) {
            R.id.car_view -> listener?.onCarSelected(view.tag as Car)
        }
    }

}