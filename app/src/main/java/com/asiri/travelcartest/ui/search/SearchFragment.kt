package com.asiri.travelcartest.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.asiri.travelcartest.R
import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.ui.helper.DelayedQueryListener
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment(), CarsListAdapter.CarsListAdapterListener {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var carsAdapter: CarsListAdapter
    private val cars = ArrayList<Car>()
    companion object{
        val CAR_CONTEXT_KEY = "CAR_CONTEXT_KEY"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        searchViewModel.initAndGetLiveData(viewLifecycleOwner)
            .observe(viewLifecycleOwner, Observer { newCars ->
                cars.clear()
                cars.addAll(newCars)
                carsAdapter.notifyDataSetChanged()
            })

        carsAdapter = CarsListAdapter(cars, this)
        car_recyclerview.adapter = carsAdapter

        car_searchview.setOnQueryTextListener(
            DelayedQueryListener(lifecycle) { newText ->
                newText?.let {
                    searchViewModel.launchCarResearch(it)
                    carsAdapter.query = newText
                }
            }
        )
    }


    override fun onCarSelected(car: Car) {
        val intent = Intent(activity, CarDetailsActivty::class.java)
        intent.putExtra(CAR_CONTEXT_KEY, car)
        startActivity(intent)
    }
}