package com.asiri.travelcartest.ui.search

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.asiri.travelcartest.App
import com.asiri.travelcartest.model.Car

class SearchViewModel : ViewModel() {

    var carsLiveData = MutableLiveData<List<Car>>()
    lateinit var repositoryCarsLiveData: LiveData<List<Car>>
    lateinit var lifecycleOwner: LifecycleOwner

    fun initAndGetLiveData(lifecycleOwner: LifecycleOwner): LiveData<List<Car>> {
        this.lifecycleOwner = lifecycleOwner
        launchCarResearch("")
        return carsLiveData
    }

    fun launchCarResearch(query: String) {
        repositoryCarsLiveData = if(query.isEmpty())
            App.carRepository.getAllCars()
        else
            App.carRepository.getCarsFromName(query)

        repositoryCarsLiveData.observe(lifecycleOwner, Observer { cars -> carsLiveData.value = cars })
    }

}