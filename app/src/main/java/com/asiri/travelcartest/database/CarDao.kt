package com.asiri.travelcartest.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.asiri.travelcartest.model.Car

/**
 * Car data access object to simplify the database queries
 */
@Dao
interface CarDao {

    //Get all the cars
    @Query("SELECT * FROM car")
    fun getAllCars() : LiveData<List<Car>>

    //Get the cars from the search
    @Query("SELECT * FROM car WHERE make LIKE :searchQuery")
    fun getCarsWhereNameContains(searchQuery: String) : LiveData<List<Car>>

    @Query("DELETE FROM car")
    fun deleteAllCars()

    //Insert a single car
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCar(cars: Car)

}