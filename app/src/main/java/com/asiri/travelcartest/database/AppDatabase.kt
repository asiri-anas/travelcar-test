package com.asiri.travelcartest.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.model.User

const val DATABASE_NAME = "travel_car_store"

/**
 * Internal database for data persistence
 */
@Database(entities = [Car::class, User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun carDao() : CarDao
    abstract fun userDao() : UserDao
}