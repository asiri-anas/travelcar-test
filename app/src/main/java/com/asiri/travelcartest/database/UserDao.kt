package com.asiri.travelcartest.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.asiri.travelcartest.model.Car
import com.asiri.travelcartest.model.User

/**
 * User data access object to simplify the database queries
 */
@Dao
interface UserDao{

    //Get the current
    @Query("SELECT * FROM user LIMIT 1")
    fun getCurrentUser() : LiveData<User>

    //Insert a single user
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    //Insert a single user
    @Update
    fun updateUser(user: User)

}