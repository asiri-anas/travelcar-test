package com.asiri.travelcartest

import android.app.Application
import androidx.room.Room
import com.asiri.travelcartest.database.AppDatabase
import com.asiri.travelcartest.database.DATABASE_NAME
import com.asiri.travelcartest.repository.CarRepository
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var db: AppDatabase
        lateinit var carRepository: CarRepository
    }

    override fun onCreate() {
        super.onCreate()

        //We initialize the logging library
        Timber.plant(Timber.DebugTree())

        //Attach the database reference to the app for global access
        db = Room.databaseBuilder(this,
            AppDatabase::class.java, DATABASE_NAME)
            .build()

        //Same for the repositories
        carRepository = CarRepository()

        carRepository.syncCarNow()

    }

}