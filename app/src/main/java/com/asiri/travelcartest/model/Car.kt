package com.asiri.travelcartest.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Car(
    @PrimaryKey(autoGenerate = true)
    var carId: Int,
    var make: String,
    var model: String,
    var year: Int,
    var picture: String,
    var equipmentsString: String) : Serializable