package com.asiri.travelcartest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true)
    var userId: Int,
    var lastname: String,
    var firstname: String,
    var address: String,
    var birthday: String
)