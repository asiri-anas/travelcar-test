package com.asiri.travelcartest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class CarFromServer (
    var carId: Int,
    var make: String,
    var model: String,
    var year: Int,
    var picture: String,
    var equipments: List<String>)